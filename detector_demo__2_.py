import sys
import time
from pathlib import Path
from typing import Optional, Tuple
from enum import Enum

import cv2
import numpy
import numpy as np
import typer

from sklearn.metrics import ConfusionMatrixDisplay

from azure_kinect_video_player.playback_wrapper import AzureKinectPlaybackWrapper
from azure_kinect_video_player.image_scaler import map_uint16_to_uint8

from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import cross_val_predict
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from skimage.feature import hog
from joblib import dump
import joblib

import seaborn as sns
from sklearn.metrics import confusion_matrix

# import 
import matplotlib.pyplot as plt

app = typer.Typer()

Visualization = False
Training = True

class ItemType(Enum):

    """
    Objects to detect
    """
    UNKNOWN = "UNKNOWN"
    BABY = "BABY"
    RABBIT = "RABBIT"
    DOG = "DOG"
    KOALA = "KOALA"
    BLOCK_MAN = "BLOCK_MAN"
    ROBOT = "ROBOT"
    TENNIS_BALL = "TENNIS_BALL"
    DINOSAUR = "DINOSAUR"
    GLASS_PAPER_WEIGHT = "GLASS_PAPER_WEIGHT"
    O2_BALL = "O2_BALL"

class DetectorType(Enum):
    """
    Detector Type Enum (none, harris, hog, sift)
    """
    NONE = "none"
    HARRIS = "harris"
    HOG = "hog"
    SIFT = "sift"

def normalize_image(image):
    res = 256
    img = image.copy()
    img = cv2.resize(img, (res, res))
    return img

@app.command()
def app_main(video_filename: Path = typer.Argument(..., help="The video filename"),
             realtime_wait: bool = typer.Option(False, help="Wait for the next frame to be displayed"),
             init_threshold: int = typer.Option(560, help="Initial threshold value"),
             detector_type: DetectorType = typer.Option(DetectorType.NONE.value, help="Detector Type")):

    # Get the video filename from the command line
    video_filename = Path(video_filename)

    # Create the playback wrapper
    playback_wrapper = AzureKinectPlaybackWrapper(video_filename,
                                                  realtime_wait=realtime_wait,
                                                  auto_start=False,
                                                  rgb=True,
                                                  depth=True,
                                                  ir=False)

    # Create windows for the depth image and feature detection
    cv2.namedWindow("Depth", cv2.WINDOW_NORMAL)
    cv2.namedWindow("Feature Detection", cv2.WINDOW_NORMAL)
    

    threshold = init_threshold

    def on_threshold_change(value):
        """
        Handler to change threshold value

        :param value: Value to set the threshold to
        """
        nonlocal threshold

        threshold = value

    # Attach Trackbar to Thresholded Depth Window
    threshold_win_name = "Thresholded Depth"
    cv2.namedWindow(threshold_win_name)
    cv2.createTrackbar("Threshold", threshold_win_name, threshold, 65536, on_threshold_change)

    # Create a mouse handler for the depth image that prints out the depth value when clicked
    def depth_click_handler(event, x, y, flags, param):
        """
        Mouse handler for Depth Image. Prints out the depth value when clicked.

        :param event: Mouse Event
        :param x: X co-ordinate
        :param y: Y co-ordinate
        :param flags: See OpenCV Docs
        :param param: Not Uses
        """
        nonlocal depth_image

        if event == cv2.EVENT_LBUTTONDOWN:
            print(f"Depth value: {depth_image[y][x]}")

    cv2.setMouseCallback("Depth", depth_click_handler)

    # Start timer
    start_time = time.time()
    playback_wrapper.start()
    
    from typing import List, Tuple

    def resize_frames(frames: List[np.ndarray], size: Tuple[int, int]=(64, 64)) -> List[np.ndarray]:
        """
        Resizes a list of frames to a specified size.

        Parameters:
            - frames (List[np.ndarray]): A list of frames to be resized.
            - size (Tuple[int, int]): A tuple specifying the desired size of the resized frames.
                Default is (64, 64).

        Returns:
            - List[np.ndarray]: A list of resized frames.
        """
        frames_resized = []
        for frame in frames:
            frame_resized = cv2.resize(frame, size)
            frames_resized.append(frame_resized)
        return frames_resized


    try:

        first_frame = True
        
        # collect frames for feature detection
        assigned_item_id = 0
        continous_frame = False
        HOGS_READY = []
        frames_resized = []
        labels = []
        alabels = []
        hog_features = []
        isTesting = False


        print("SVM Data loaded")

        # load in clf
        loaded_sgd_clf = joblib.load("classifier.pkl")
        loaded_scaler = joblib.load("scaler.pkl")
        
        
        # Loop through the frames
        for colour_image, depth_image, ir_image in playback_wrapper.grab_frame():

            # If all images are None, break (probably reached the end of the video)
            if colour_image is None and depth_image is None and ir_image is None:
                break

            # Threshold the depth image
            _, thresholded_depth_image = cv2.threshold(depth_image, threshold, 65536, cv2.THRESH_BINARY)

            # Create a mask for the portion of the depth image that is out of the sensor
            if first_frame:
                out_of_sensor_mask = numpy.ma.masked_where(thresholded_depth_image == 0, thresholded_depth_image)
                first_frame = False

            # Get the points where the depth image is 0 (should be object)
            points = numpy.argwhere(thresholded_depth_image == 0)

            # Select the points that are not outside of the sensor region
            bb_points = points[~out_of_sensor_mask.mask[points[:, 0], points[:, 1]]]

            # Convert the depth image to a 8-bit image, then to a colour image
            depth_image_8bit = map_uint16_to_uint8(depth_image, 0, 10000)
            depth_image_colour = cv2.cvtColor(depth_image_8bit, cv2.COLOR_GRAY2BGR)

            depth_image_thresholded_8bit = map_uint16_to_uint8(thresholded_depth_image, 0, 65535)
            depth_image_thresholded_colour = cv2.cvtColor(depth_image_thresholded_8bit, cv2.COLOR_GRAY2BGR)

            # Draw the out of sensor mask on the depth image
            depth_image_colour[out_of_sensor_mask.mask] = (0, 0, 255)
            depth_image_thresholded_colour[out_of_sensor_mask.mask] = (0, 0, 255)

            if len(bb_points) > 0:
                # Get the bounding box
                y, x, h, w = cv2.boundingRect(bb_points)

                # Draw the bounding box if it exists or is smaller than the image
                if w > 0 and h > 0 and w < depth_image.shape[1] and h < depth_image.shape[0]:
                    cv2.rectangle(depth_image_thresholded_colour, (x, y), (x + w, y + h), (0, 255, 0), 2)
                    cv2.rectangle(depth_image_colour, (x, y), (x + w, y + h), (0, 255, 0), 2)
                    
                    # Crop the frame where the object is located
                    cropped_frame = depth_image_thresholded_colour[y:y + h, x:x + w]

                    # Convert the cropped frame to grayscale
                    cropped_frame_gray = cv2.cvtColor(cropped_frame, cv2.COLOR_BGR2GRAY)

                    cropped_frame_resized = cv2.resize(cropped_frame_gray, (128, 128))

                    # # Extract HOG features
                    # hog = cv2.HOGDescriptor()
                    # hog_features = hog.compute(cropped_frame_resized)
                    # hog_features = hog_features.reshape(-1, 1)

                    # hog_features, hog_window = hog(cropped_frame_resized, orientations=9, pixels_per_cell=(8,8), cells_per_block=(2,2), visualize=False)
                    hog_features = np.array(hog(cropped_frame_resized, orientations=9, pixels_per_cell=(8,8), cells_per_block=(2,2), visualize=False))

                    # cv2.imshow("HOG", hog_window)
                    # hog_features = hog_features.reshape(-1, 1)
                    
                    # print('Shape of hog_features:', hog_features)
                    # if video is training set
                    if Path("kinect-training-set.mkv") == video_filename:
                        print("Training")
                        if playback_wrapper.get_current_frame_number() >= 239 and playback_wrapper.get_current_frame_number() <= 569: #From frames 240 to frame 570 it's BABY
                            labels.append(0)
                            HOGS_READY.append(hog_features)
                        elif playback_wrapper.get_current_frame_number() >= 720 and playback_wrapper.get_current_frame_number() <= 1061: #From frames 720 to frame 1060 it's RABBIT
                            labels.append(1)
                            HOGS_READY.append(hog_features)
                        elif playback_wrapper.get_current_frame_number() >= 1230 and playback_wrapper.get_current_frame_number() <= 1594: #From frames 720 to frame 1060 it's DOG
                            labels.append(2)
                            HOGS_READY.append(hog_features)
                        elif playback_wrapper.get_current_frame_number() >= 1792 and playback_wrapper.get_current_frame_number() <= 2128: #From frames 720 to frame 1060 it's RABBIT
                            labels.append(3)
                            HOGS_READY.append(hog_features)
                        elif playback_wrapper.get_current_frame_number() >= 2368 and playback_wrapper.get_current_frame_number() <= 2695:
                            labels.append(4)
                            HOGS_READY.append(hog_features)
                        elif playback_wrapper.get_current_frame_number() >= 2807 and playback_wrapper.get_current_frame_number() <= 3542:
                            labels.append(5)
                            HOGS_READY.append(hog_features)
                        elif playback_wrapper.get_current_frame_number() >= 3542 and playback_wrapper.get_current_frame_number() <= 3889:
                            labels.append(6)
                            HOGS_READY.append(hog_features)
                        elif playback_wrapper.get_current_frame_number() >= 4053 and playback_wrapper.get_current_frame_number() <= 4421:
                            labels.append(7)
                            HOGS_READY.append(hog_features)
                        elif playback_wrapper.get_current_frame_number() >= 4656 and playback_wrapper.get_current_frame_number() <= 5082:
                            labels.append(8)
                            HOGS_READY.append(hog_features)
                        elif playback_wrapper.get_current_frame_number() >= 5264 and playback_wrapper.get_current_frame_number() <= 5656:
                            labels.append(9)
                            HOGS_READY.append(hog_features)
                        # else:
                        #     labels.append(-1)  # Use a negative label to indicate an unknown class
                        #     HOGS_READY.append(hog_features)       
  
                    else:
                        isTesting = True
                        if playback_wrapper.get_current_frame_number() >= 300 and playback_wrapper.get_current_frame_number() <= 650:
                            alabels.append(5)
                            hog_features_scaled_test = loaded_scaler.transform([hog_features])
                            labels.append(loaded_sgd_clf.predict(hog_features_scaled_test)[0])
                        elif playback_wrapper.get_current_frame_number() >= 950 and playback_wrapper.get_current_frame_number() <= 1300:
                            alabels.append(3)
                            hog_features_scaled_test = loaded_scaler.transform([hog_features])
                            labels.append(loaded_sgd_clf.predict(hog_features_scaled_test)[0])
                        elif playback_wrapper.get_current_frame_number() >= 1500 and playback_wrapper.get_current_frame_number() <= 1850:
                            alabels.append(9)
                            hog_features_scaled_test = loaded_scaler.transform([hog_features])
                            labels.append(loaded_sgd_clf.predict(hog_features_scaled_test)[0])
                        elif playback_wrapper.get_current_frame_number() >= 2100 and playback_wrapper.get_current_frame_number() <= 2450:
                            alabels.append(4)
                            hog_features_scaled_test = loaded_scaler.transform([hog_features])
                            labels.append(loaded_sgd_clf.predict(hog_features_scaled_test)[0])
                        elif playback_wrapper.get_current_frame_number() >= 2650 and playback_wrapper.get_current_frame_number() <= 3050:
                            alabels.append(8)
                            hog_features_scaled_test = loaded_scaler.transform([hog_features])
                            labels.append(loaded_sgd_clf.predict(hog_features_scaled_test)[0])
                        elif playback_wrapper.get_current_frame_number() >= 3300 and playback_wrapper.get_current_frame_number() <= 3650:
                            alabels.append(0)
                            hog_features_scaled_test = loaded_scaler.transform([hog_features])
                            labels.append(loaded_sgd_clf.predict(hog_features_scaled_test)[0])
                        elif playback_wrapper.get_current_frame_number() >= 3850 and playback_wrapper.get_current_frame_number() <= 4200:
                            alabels.append(7)
                            hog_features_scaled_test = loaded_scaler.transform([hog_features])
                            labels.append(loaded_sgd_clf.predict(hog_features_scaled_test)[0])
                        elif playback_wrapper.get_current_frame_number() >= 4350 and playback_wrapper.get_current_frame_number() <= 4750:
                            alabels.append(1)
                            hog_features_scaled_test = loaded_scaler.transform([hog_features])
                            labels.append(loaded_sgd_clf.predict(hog_features_scaled_test)[0])
                        elif playback_wrapper.get_current_frame_number() >= 4950 and playback_wrapper.get_current_frame_number() <= 5300:
                            alabels.append(10)
                            hog_features_scaled_test = loaded_scaler.transform([hog_features])
                            labels.append(loaded_sgd_clf.predict(hog_features_scaled_test)[0])
                        elif playback_wrapper.get_current_frame_number() >= 5450 and playback_wrapper.get_current_frame_number() <= 5850: 
                            alabels.append(2)
                            hog_features_scaled_test = loaded_scaler.transform([hog_features])
                            labels.append(loaded_sgd_clf.predict(hog_features_scaled_test)[0])

                        hog_features_scaled_test = loaded_scaler.transform([hog_features])
                        print(loaded_sgd_clf.predict(hog_features_scaled_test)[0])
                        # labels.append(loaded_sgd_clf.predict(hog_features_scaled_test))


                # Draw the bb_points in blue on the depth image
                depth_image_cropped_frame = depth_image_colour[y:y + h, x:x + w]
                depth_image_colour[bb_points[:, 0], bb_points[:, 1]] = (255, 0, 0)

                # Draw the bb_points in blue on the depth image
                depth_image_colour[bb_points[:, 0], bb_points[:, 1]] = (255, 0, 0)

                #####################################################
                ### MODIFY BOUNDING BOX TO MATCH RGB IMAGE REGION ###
                #####################################################

                # Crop the rgb image to the bounding box
                cropped_rgb_image = colour_image[int(y + 1):int(y + h - 1), int(x + 1):int(x + w - 1), :]

                # If we have a detector, run it
                updated_image = None

                if detector_type != DetectorType.NONE:
                    if detector_type == DetectorType.HARRIS:
                        updated_image = harris_corners(cropped_rgb_image)
                    elif detector_type == DetectorType.HOG:
                        hog_features = hog_histogram(cropped_rgb_image)
                        updated_image = None
                    elif detector_type == DetectorType.SIFT:
                        updated_image = sift_keypoints(cropped_rgb_image)
                    else:
                        raise ValueError("Unknown Detector Type")

                # If we have an updated image, merge it with the original image and display it
                new_colour_image = colour_image.copy()

                if updated_image is not None:
                    new_colour_image[int(y + 1):int(y + h - 1), int(x + 1):int(x + w - 1), :] = updated_image
                    cv2.rectangle(depth_image_colour, (x, y), (x + w, y + h), (0, 255, 0), 2)

                # Draw the bounding box if it exists or is smaller than the image
                if w > 0 and h > 0 and w < depth_image.shape[1] and h < depth_image.shape[0]:
                    cv2.rectangle(new_colour_image, (x, y), (x + w, y + h), (0, 255, 0), 2)

                cv2.imshow("Feature Detection", new_colour_image)

            else:
                continous_frame = False
                # If we have no points, display the original image
                cv2.imshow("Feature Detection", colour_image)

            cv2.imshow("Depth", depth_image_colour)
            cv2.imshow(threshold_win_name, thresholded_depth_image)

            # Wait for key press
            key = cv2.waitKey(1)

            # If q or ESC is pressed, break
            if key == ord("q") or key == 27:
                break

    except KeyboardInterrupt:
        pass
    
    if isTesting == False:
        # print("Size of labels:", len(labels))
        # print("Size of hog_features:", hog_features.shape)
        # print('Shape of hog_features_scaled:', hog_features.shape)
        # print('Shape of labels:', len(labels))

        sgd_clf = SGDClassifier(random_state=42, max_iter=1000, tol=1e-3)
        sgd_clf.fit(HOGS_READY, labels)
        print('Shape of classifier coefficients:', sgd_clf.coef_.shape)     # Print the shape of the classifier coefficients

        scaler = StandardScaler()
        hog_features_scaled = scaler.fit(HOGS_READY)

        joblib.dump(sgd_clf, 'classifier.pkl')    # Save the classifier to a file
        joblib.dump(scaler, 'scaler.pkl')         # Save the scaler to a file

        print("Saved classifier and scaler to file")
    else:
        labelnames = ['BABY', 'RABBIT', 'DOG', 'KOALA', 'BLOCK_MAN', 'ROBOT', 'TENNIS_BALL', 'DINOSAUR', 'GLASS_PAPER_WEIGHT', 'O2_BALL']

        title1 = "Confusion Matrix"
        # ConfusionMatrixDisplay.from_predictions(alabels, labels, display_labels=labelnames).plot()
        # compute confusion matrix
        cm = confusion_matrix(alabels, labels)

        # plot confusion matrix
        sns.heatmap(cm, annot=True, cmap='Blues', fmt='g')
        plt.xlabel('Predicted labels')
        plt.ylabel('True labels')
        plt.show()

        plt.title(title1) # title not working for some reason
        plt.show()
        
    # Stop timer
    end_time = time.time()

    # Print the time taken
    print("Time taken: {}s".format(end_time - start_time))

    # Close the windows
    cv2.destroyAllWindows()

    # Stop the playback wrapper
    playback_wrapper.stop()

    return 0


def harris_corners(image: numpy.ndarray) -> Optional[numpy.ndarray]:
    """
    Detects Harris Corners and returns an image of corners

    Based on: https://www.geeksforgeeks.org/feature-detection-and-matching-with-opencv-python/

    :param image:
    :return modified image showing Harris corners
    """

    if len(image.shape) == 0 or image.shape[0] == 0 or image.shape[1] == 0:
        return None

    # Convert the image to a floating point greyscale image
    grey_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    grey_image = numpy.float32(grey_image)

    # Apply harris corners
    corner_image = cv2.cornerHarris(grey_image, blockSize=2, ksize=3, k=0.04)

    # Dilate to mark image corners
    corner_image_dilate = cv2.dilate(corner_image, None)

    # Mark corners on image
    return_value = numpy.copy(image)
    return_value[corner_image_dilate > 0.01 * corner_image_dilate.max()] = [0, 255, 0]

    return return_value


def hog_histogram(image: numpy.ndarray, nbins: int = 9, display: bool = True) -> Optional[numpy.ndarray]:
    """
    Calculates the HOG histogram for the given image

    Based on: https://stackoverflow.com/a/37005437/791025

    :param image: Image to detect features in
    :param nbins: Number of bins to use in the histogram
    :param display: Whether to display the histogram
    :return: The HOG histogram
    """

    if len(image.shape) == 0 or image.shape[0] == 0 or image.shape[1] == 0:
        return None

    # Convert the image to a greyscale image
    grey_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Create the HOG detector
    cell_size = (8, 8)
    block_size = (2, 2)

    # Create the HOG descriptor
    hog_detector = cv2.HOGDescriptor(_winSize=(grey_image.shape[1] // cell_size[1] * cell_size[1],
                                               grey_image.shape[0] // cell_size[0] * cell_size[0]),
                                     _blockSize=(block_size[1] * cell_size[1], block_size[0] * cell_size[0]),
                                     _blockStride=(cell_size[1], cell_size[0]),
                                     _cellSize=(cell_size[1], cell_size[0]),
                                     _nbins=nbins)

    try:
        # Compute the features
        hog_features_cv = hog_detector.compute(grey_image)
    except cv2.error:
        # Return None in case of error
        return None

    if display and len(hog_features_cv) > 0:
        # If we are displaying the histogram, create a histogram image

        n_cells = (grey_image.shape[0] // cell_size[0], grey_image.shape[1] // cell_size[1])
        hog_features = hog_features_cv.reshape(n_cells[1] - block_size[1] + 1, n_cells[0] - block_size[0] + 1,
                                               block_size[0], block_size[1], nbins).transpose(
                                                   (1, 0, 2, 3, 4))  # index blocks by rows first

        # hog_feats now contains the gradient amplitudes for each direction,
        # for each cell of its group for each group. Indexing is by rows then columns.

        gradients = numpy.zeros((n_cells[0], n_cells[1], nbins))

        # count cells (border cells appear less often across overlapping groups)
        cell_count = numpy.full((n_cells[0], n_cells[1], 1), 0, dtype=int)

        for off_y in range(block_size[0]):
            for off_x in range(block_size[1]):
                gradients[off_y:n_cells[0] - block_size[0] + off_y + 1,
                off_x:n_cells[1] - block_size[1] + off_x + 1] += \
                    hog_features[:, :, off_y, off_x, :]
                cell_count[off_y:n_cells[0] - block_size[0] + off_y + 1,
                           off_x:n_cells[1] - block_size[1] + off_x + 1] += 1

        # Average gradients
        gradients /= cell_count

        # Plot the histogram
        show_image = gradients[:, :, 5]
        resized_gradients = cv2.resize(show_image, (show_image.shape[1] * 4, show_image.shape[0] * 4))

        # Display the histogram
        cv2.imshow("HOG Descriptor", resized_gradients)

    return hog_features_cv


sift_detector = None


def sift_keypoints(image: numpy.ndarray) -> Optional[Tuple[numpy.ndarray, numpy.ndarray, numpy.ndarray]]:
    """
    Detects SIFT keypoints in the given image

    :param image: Image to detect features in
    :return: The image with keypoints drawn, the keypoints, and the descriptors
    """
    global sift_detector

    # Check if the image is valid
    if len(image.shape) == 0 or image.shape[0] == 0 or image.shape[1] == 0:
        return None

    # Convert the image to a greyscale image
    grey_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Create the SIFT detector (stored in a global variable)
    if not sift_detector:
        sift_detector = cv2.SIFT_create()

    # Detect keypoints and compute descriptors
    kp, des = sift_detector.detectAndCompute(grey_image, None)

    # Draw keypoints
    kp_image = cv2.drawKeypoints(image, kp, None, color=(0, 255, 0), flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

    return kp_image, kp, des


if __name__ == "__main__":
    sys.exit(app())
